package facci.pm.rodriguezchoez.siga.models;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JasonMateriasApi {


        @GET("materias")
        Call<List<Posts>> getPosts();

}
