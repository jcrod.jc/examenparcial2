package facci.pm.rodriguezchoez.siga.models;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class Posts {
    private Context context;
    private int id;
    private String descripcion;
    private int parcial_uno;
    private int parcial_dos;
    private String imagen;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getParcial_uno() {
        return parcial_uno;
    }

    public void setParcial_uno(int parcial_uno) {
        this.parcial_uno = parcial_uno;
    }

    public int getParcial_dos() {
        return parcial_dos;
    }

    public void setParcial_dos(int parcial_dos) {
        this.parcial_dos = parcial_dos;
    }

    public String getImagen() {


        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
