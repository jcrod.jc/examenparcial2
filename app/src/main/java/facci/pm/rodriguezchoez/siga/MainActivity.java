package facci.pm.rodriguezchoez.siga;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import facci.pm.rodriguezchoez.siga.models.JasonMateriasApi;
import facci.pm.rodriguezchoez.siga.models.MateriasREspuesta;
import facci.pm.rodriguezchoez.siga.models.Posts;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "Materias";

    private TextView mJsonTxtView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mJsonTxtView = findViewById(R.id.jsonText);
        getPosts();
    }

    private void getPosts() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.32.6.242:3005/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JasonMateriasApi jsonPlaceHolderApi = retrofit.create(JasonMateriasApi.class);

        Call<List<Posts>> call = jsonPlaceHolderApi.getPosts();


        call.enqueue(new Callback<List<Posts>>() {
            @Override
            public void onResponse(Call<List<Posts>> call, Response<List<Posts>> response) {

                if(!response.isSuccessful()){
                    mJsonTxtView.setText("Codigo: "+response.code());
                    return;
                }

                List<Posts> postsList = response.body();

                for(Posts post: postsList){
                    String content = "";
                    content += "Materia: "+  post.getDescripcion() + "\n";
                    content += "Nota Primer Parcial: "+ post.getParcial_uno() + "\n";
                    content +=  "Nota Segundo Parcial: "+ post.getParcial_dos() + "\n";
                    content +=   post.getImagen() + "\n\n";
                    mJsonTxtView.append(content);

                }


            }

            @Override
            public void onFailure(Call<List<Posts>> call, Throwable t) {
                mJsonTxtView.setText(t.getMessage());

            }
        });

    }
}