package facci.pm.rodriguezchoez.siga.models;

import java.util.ArrayList;

public class MateriasREspuesta {

    ArrayList<Posts> results;

    public ArrayList<Posts> getResults() {
        return results;
    }

    public void setResults(ArrayList<Posts> results) {
        this.results = results;
    }
}
